package scotlandyard;

import java.util.*;

/**
 * A class to perform all of the game logic.
 */

public class ScotlandYard implements ScotlandYardView, Receiver {

    protected MapQueue<Integer,Token> queue;
    protected Integer gameId;
    protected Random random;
    private Integer numberOfDetectives;
    protected List<Boolean> rounds;
    protected ScotlandYardGraph graph;
    private PlayerData currentPlayer;
    private Integer mrXLocation;
    private Integer knownMrXLocation;
    private Integer noOfPlayers;
    protected List<PlayerData> players;
    protected Integer round;
    private List<Spectator> spectators;
    private Set<Colour> setOfWinners;
    /**
     * Constructs a new ScotlandYard object. This is used to perform all of the game logic.
     *
     * @param numberOfDetectives the number of detectives in the game.
     * @param rounds the List of booleans determining at which rounds Mr X is visible.
     * @param graph the graph used to represent the board.
     * @param queue the Queue used to put pending moves onto.
     * @param gameId the id of this game.
     */
    public ScotlandYard(Integer numberOfDetectives, List<Boolean> rounds, ScotlandYardGraph graph, MapQueue<Integer, Token> queue, Integer gameId) {
        this.queue = queue;
        this.gameId = gameId;
        this.random = new Random();
        this.players = new ArrayList<PlayerData>();
        this.numberOfDetectives = numberOfDetectives;
        this.rounds = rounds;
        this.graph = graph;
        this.knownMrXLocation = 0;
        this.noOfPlayers = 0;
        this.round = 0;
        this.spectators = new ArrayList<Spectator>();
        this.setOfWinners = new HashSet<Colour>();

    }

    /**
     * Starts playing the game.
     */
    public void startRound() {
        if (isReady() && !isGameOver()) {
            turn();
        }
    }

    /**
     * Notifies a player when it is their turn to play.
     */
    public void turn() {
        Integer token = getSecretToken();
        queue.put(gameId, new Token(token, getCurrentPlayer(), System.currentTimeMillis()));
        notifyPlayer(getCurrentPlayer(), token);
        getKnownMrXLocation(getPlayer(Colour.Black));
    }

    /**
     * Plays a move sent from a player.
     *
     * @param move the move chosen by the player.
     * @param token the secret token which makes sure the correct player is making the move.
     */
    public void playMove(Move move, Integer token) {
        Token secretToken = queue.get(gameId);
        if (secretToken != null && token == secretToken.getToken()) {
            queue.remove(gameId);
            play(move);
            nextPlayer();
            startRound();
        }
    }

    /**
     * Returns a random integer. This is used to make sure the correct player
     * plays the move.
     * @return a random integer.
     */
    private Integer getSecretToken() {
        return random.nextInt();
    }

    /**
     * Notifies a player with the correct list of valid moves.
     *
     * @param colour the colour of the player to be notified.
     * @param token the secret token for the move.
     */
    private void notifyPlayer(Colour colour, Integer token) {
        List<Move> moves = validMoves(colour);
        Player p = getPlayer(colour).getPlayer();
        p.notify(getPlayer(colour).getLocation(), moves, token, this);
    }

    /**
     * Passes priority onto the next player whose turn it is to play.
     */
    protected void nextPlayer() {
        int playerPos = players.indexOf(currentPlayer);
        if(playerPos == players.size()-1) {
            currentPlayer = players.get(0);
        }
        else {
            currentPlayer = players.get(playerPos+1);
        }
    }

    /**
     * Allows the game to play a given move.
     *
     * @param move the move that is to be played.
     */
    protected void play(Move move) {
        if (move instanceof MoveTicket) play((MoveTicket) move);
        else if (move instanceof MoveDouble) play((MoveDouble) move);
        else if (move instanceof MovePass) play((MovePass) move);
    }

    /**
     * Plays a MoveTicket.
     *
     * @param move the MoveTicket to play.
     */
    protected void play(MoveTicket move) {
        currentPlayer.setLocation(move.target);
        currentPlayer.removeTicket(move.ticket);
        if(currentPlayer.getColour() != Colour.Black) {
            getPlayer(Colour.Black).addTicket(move.ticket);
        }
        notifySpectators(move);
        if(currentPlayer.getColour() == Colour.Black) {
            round = round +1;
        }
    }

    /**
     * Plays a MoveDouble.
     *
     * @param move the MoveDouble to play.
     */
    protected void play(MoveDouble move) {
        notifySpectators(move);
        play(move.move1);
        notifySpectators(move);
        play(move.move2);
        currentPlayer.removeTicket(Ticket.Double);
    }

    /**
     * Plays a MovePass.
     *
     * @param move the MovePass to play.
     */
    protected void play(MovePass move) {
        currentPlayer.setLocation(currentPlayer.getLocation());
        notifySpectators(move);
    }

    /**
     * Returns the list of valid moves for a given player @param the player whose moves we want to see.
     * @return the list of valid moves for a given player.
     */
    private List<Move> validMoves(Colour colour) {
        PlayerData player = getPlayer(colour);
        int location = player.getLocation();
        Map<Ticket, Integer> tickets = player.getTickets();
        List<Move> validMoves = graph.generateMoves(colour, location, tickets);
        validMoves = removeOccupiedNodes(validMoves);
        if ((validMoves.isEmpty()) && (colour != Colour.Black)){
            validMoves.add(MovePass.instance(colour));
        }
        return validMoves;
    }

    private List<Move> removeOccupiedNodes(List<Move> moves) {

        List<Move> newMoves = new ArrayList<Move>();                                                    //Remove a Move if the destination is on a detective's location
        newMoves.addAll(moves);
        for(Move move : moves){
            if(move instanceof MoveTicket) {
                for(PlayerData player : players) {
                    if ((player.getColour() != Colour.Black) && (player.getLocation().equals(((MoveTicket) move).target))) {
                        newMoves.remove(move);
                    }
                }
            }
            if(move instanceof MoveDouble) {                                        //If double move, make list with both single moves and remove the entire move
                List<Move> moveCheck = new ArrayList<Move>();                          // if either ticket is not a valid move
                moveCheck.add(((MoveDouble) move).move1);
                moveCheck.add(((MoveDouble) move).move2);
                moveCheck = removeOccupiedNodes(moveCheck);
                if (moveCheck.size() != 2){
                    newMoves.remove(move);
                }
            }
        }
        return newMoves;
    }

    /**
     * Allows spectators to join the game. They can only observe as if they
     * were a detective: only MrX's revealed locations can be seen.
     *
     * @param spectator the spectator that wants to be notified when a move is made.
     */
    public void spectate(Spectator spectator) {
        spectators.add(spectator);
    }

    private void notifySpectators(Move move) {
        if(spectators.isEmpty()) {
            return;
        }
        else if(currentPlayer.getColour() == Colour.Black && !rounds.get(getRound()) && knownMrXLocation == 0 && move instanceof  MoveTicket) {
            MoveTicket tempMove = MoveTicket.instance(Colour.Black, ((MoveTicket) move).ticket, knownMrXLocation);
            notifyAllSpecs(tempMove);
        }
        else {
            notifyAllSpecs(move);
        }
    }

    private void notifyAllSpecs(Move move) {
        for (Spectator spectator : spectators) {
            spectator.notify(move);
        }
    }

    /**
     * Allows players to join the game with a given starting state. When the
     * last player has joined, the game must ensure that the first player to play is Mr X.
     *
     * @param player the player that wants to be notified when he must make moves.
     * @param colour the colour of the player.
     * @param location the starting location of the player.
     * @param tickets the starting tickets for that player.
     * @return true if the player has joined successfully.
     */
    public boolean join(Player player, Colour colour, int location, Map<Ticket, Integer> tickets) { //done
        PlayerData newPlayer = new PlayerData(player, colour, location, tickets);
        if (!players.isEmpty()) {
            for (PlayerData p : players){                                                        //eliminate possibility of new players sharing colours or locations
                if (newPlayer.getColour().equals(p.getColour()) || newPlayer.getLocation().equals(p.getLocation())){
                    return false;
                }
            }
        }
        if (newPlayer.getColour().equals(Colour.Black)){
            players.add(0, newPlayer);
            mrXLocation = location;
            currentPlayer = newPlayer;
        }
        else{
            players.add(newPlayer);
        }
        noOfPlayers = noOfPlayers + 1;
        return true;
    }

    /**
     * A list of the colours of players who are playing the game in the initial order of play.
     * The length of this list should be the number of players that are playing,
     * the first element should be Colour.Black, since Mr X always starts.
     *
     * @return The list of players.
     */
    public List<Colour> getPlayers() { //done
        List<Colour> colours = new ArrayList<Colour>();
        for (PlayerData player : players){
            colours.add(player.getColour());
        }
        return colours;
    }

    /**
     * Returns the colours of the winning players. If Mr X it should contain a single
     * colour, else it should send the list of detective colours
     *
     * @return A set containing the colours of the winning players
     */
    public Set<Colour> getWinningPlayers() {
        isGameOver();
        return setOfWinners;
    }

    /**
     * The location of a player with a given colour in its last known location.
     *
     * @param colour The colour of the player whose location is requested.
     * @return The location of the player whose location is requested.
     * If Black, then this returns 0 if MrX has never been revealed,
     * otherwise returns the location of MrX in his last known location.
     * MrX is revealed in round n when {@code rounds.get(n)} is true.
     */
    public int getPlayerLocation(Colour colour) { //done

        PlayerData player = getPlayer(colour);
        if (player == null){
            return -1;
        }
        if(colour == Colour.Black) {
            return getKnownMrXLocation(player);
        }
        return player.getLocation();
    }

    private int getKnownMrXLocation(PlayerData player) {
        if(rounds.get(round)) {
            knownMrXLocation = player.getLocation();
        }
        mrXLocation = player.getLocation();
        return knownMrXLocation;
    }

    /**
     * The number of a particular ticket that a player with a specified colour has.
     *
     * @param colour The colour of the player whose tickets are requested.
     * @param ticket The type of tickets that is being requested.
     * @return The number of tickets of the given player.
     */
    public int getPlayerTickets(Colour colour, Ticket ticket) { //done
        PlayerData player = getPlayer(colour);
        if (player == null){
            return -1;
        }
        Map<Ticket, Integer> tickets = player.getTickets();
        return tickets.get(ticket);
    }

    /**
     * The game is over when MrX has been found or the agents are out of
     * tickets. See the rules for other conditions.
     *
     * @return true when the game is over, false otherwise.
     */
    public boolean isGameOver() {
        setOfWinners.clear();
        if(getRound()+1 >= rounds.size() && currentPlayer.getColour() == Colour.Black) {
            setOfWinners.add(Colour.Black);
            return true;
        }
        int strandedPlayers = 0;
        for(PlayerData player : players) {
            if(player.getLocation().equals(mrXLocation) && player.getColour() != Colour.Black) {
                addDetectivesToWin();
                return true;
            }
            if((validMoves(player.getColour())).size() == 0 && player.getColour() == Colour.Black) {
                addDetectivesToWin();
                return true;
            }
            if(validMoves(player.getColour()).size() == 1 && player.getColour() != Colour.Black) {
                strandedPlayers = strandedPlayers+1;
            }
        }
        if(strandedPlayers == numberOfDetectives) {
            setOfWinners.add(Colour.Black);
            return true;
        }
        return false;
    }

    private void addDetectivesToWin() {
        for(PlayerData p : players) {
            if(p.getColour() != Colour.Black) {
                setOfWinners.add(p.getColour());
            }
        }
    }


    /**
     * A game is ready when all the required players have joined.
     *
     * @return true when the game is ready to be played, false otherwise.
     */
    public boolean isReady() {
        if ((noOfPlayers == numberOfDetectives + 1) && (players.get(0).getColour().equals(Colour.Black)))  {
            return true;
        }
        return false;
    }

    /**
     * The player whose turn it is.
     *
     * @return The colour of the current player.
     */
    public Colour getCurrentPlayer() { //done
        return currentPlayer.getColour();
    }

    /**
     * The round number is determined by the number of moves MrX has played.
     * Initially this value is 0, and is incremented for each move MrX makes.
     * A double move counts as two moves.
     *
     * @return the number of moves MrX has played.
     */
    public int getRound() { //done
        return round;
    }

    /**
     * A list whose length-1 is the maximum number of moves that MrX can play in a game.
     * The getRounds().get(n) is true when MrX reveals the target location of move n,
     * and is false otherwise.
     * Thus, if getRounds().get(0) is true, then the starting location of MrX is revealed.
     *
     * @return a list of booleans that indicate the turns where MrX reveals himself.
     */
    public List<Boolean> getRounds() {
        return rounds;
    }

    public PlayerData getPlayer(Colour colour){ //done
        for (PlayerData player : players){
            if (player.getColour() == (colour)){
                return player;
            }
        }
        return null;
    }
}