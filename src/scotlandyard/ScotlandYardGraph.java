package scotlandyard;

import graph.*;

import java.util.*;

public class ScotlandYardGraph extends UndirectedGraph<Integer, Transport> {
    //this method takes in a location and generates a list of valid moves from that location based on a few properties:
    //whether the player has >0 of each type of normal ticket (tickets)
    //the player's colour (rules are different for mr x since he can use secret and x2 tickets) (colour) (colour is also required to make moves)

    //if colour == black then;
    //if the player has >0 secret tickets to travel boats (and all transport) with (tickets)
    //if the player has >0 x2 tickets (tickets)
    //if the player has a x2 ticket which is being used and thus the method is being called recursively (tickets)

    //(in which case, further recursion is stopped and the method needs to be called on a new location with 1 fewer

    public List<Move> generateMoves(Colour colour, int location, Map<Ticket, Integer> tickets) {

        List<Move> validmoves = new ArrayList<Move>();                                                          //create a list of valid moves

        List<MoveTicket> validmovetickets = generateSingleMoves(colour, location, tickets);         //generate a list of valid single moves
        validmoves.addAll(validmovetickets);                                                        //add the list of single moves to the list of valid moves

        if ((colour == Colour.Black) && (tickets.get(Ticket.Double) > 0)){                                  //if player is mr. x and has double tickets then create
            for (MoveTicket move1 : validmovetickets){                                                      //valid double moves for each valid single move
                Map<Ticket, Integer> newtickets = new HashMap<Ticket, Integer>();
                newtickets.putAll(tickets);
                newtickets.put(move1.ticket, (tickets.get(move1.ticket)-1));
                List<MoveTicket> recursemovetickets = generateSingleMoves(colour, move1.target, newtickets);
                for (MoveTicket move2 : recursemovetickets){
                    MoveDouble movedouble = MoveDouble.instance(colour, move1, move2);
                    validmoves.add(movedouble);
                }
            }
        }
        return validmoves;
    }

    public List<MoveTicket> generateSingleMoves(Colour colour, int location, Map<Ticket, Integer> tickets) {

        List<Edge<Integer,Transport>> connectedEdges = getEdgesFrom(getNode(location));

        Boolean taxi, bus, underground;
        taxi = (tickets.get(Ticket.Taxi) > 0);
        bus = (tickets.get(Ticket.Bus) > 0);
        underground = (tickets.get(Ticket.Underground) > 0);

        List<MoveTicket> validMoveTickets = new ArrayList<MoveTicket>();
        for (Edge<Integer,Transport> edge : connectedEdges){
            if ((edge.getData() == Transport.Taxi) && (taxi) ){
                MoveTicket move = MoveTicket.instance(colour, Ticket.Taxi, edge.getTarget().getIndex());
                validMoveTickets.add(move);
            }
            else if ((edge.getData() == Transport.Bus) && (bus) ){
                MoveTicket move = MoveTicket.instance(colour, Ticket.Bus, edge.getTarget().getIndex());
                validMoveTickets.add(move);
            }
            else if ((edge.getData() == Transport.Underground) && (underground) ){
                MoveTicket move = MoveTicket.instance(colour, Ticket.Underground, edge.getTarget().getIndex());
                validMoveTickets.add(move);
            }
        }
        if ((colour == Colour.Black) && (tickets.get(Ticket.Secret) > 0)){
            for (Edge<Integer,Transport> edge : connectedEdges){
                MoveTicket move = MoveTicket.instance(colour, Ticket.Secret, edge.getTarget().getIndex());
                validMoveTickets.add(move);

            }
        }
        return validMoveTickets;
    }

}